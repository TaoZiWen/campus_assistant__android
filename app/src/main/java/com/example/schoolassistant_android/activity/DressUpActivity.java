package com.example.schoolassistant_android.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.graphics.Rect;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageButton;

import com.example.schoolassistant_android.Adapter.DressUpAdapter;
import com.example.schoolassistant_android.R;
import com.example.schoolassistant_android.base.BaseActivity;
import com.example.schoolassistant_android.domain.DressUpData;
import com.example.schoolassistant_android.utils.ImageResourceUtil;
import com.example.schoolassistant_android.utils.MMKVUtils;
import com.example.schoolassistant_android.view.listener.ItemClickListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * @autho Administrator
 * @type DressUpActivity
 * @creattime 2020/8/25 11:59
 * @Desction 个性装扮页面
 */
public class DressUpActivity extends BaseActivity implements ItemClickListener {

    private static final String TAG = "DressUpActivity:";

    private List<DressUpData> list;//存放图片列表

    @Bind(R.id.recyclerView)
    RecyclerView recyclerView;
    @Bind(R.id.imgbtn_back)
    ImageButton mImgBtnBack;

    private DressUpAdapter dressUpAdapter;//个性装饰适配器

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dress_up);
        initData();
        initView();
        initListener();
    }

    @Override
    protected void initData() {
        ButterKnife.bind(this);
        list = new ArrayList<>();
        initImageList();
    }

    @Override
    protected void initView() {
        dressUpAdapter = new DressUpAdapter(this,list);
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL);
        staggeredGridLayoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_NONE);//解决item跳动
        recyclerView.setLayoutManager(staggeredGridLayoutManager);//设置layoutManager
        recyclerView.setItemAnimator(new DefaultItemAnimator()); // 默认动画
        recyclerView.addItemDecoration(new SpacesItemDecoration(20));//设置其中item间距
        recyclerView.setAdapter(dressUpAdapter);



    }

    @Override
    protected void initListener() {
        dressUpAdapter.setOnItemClickListener(this);
        mImgBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    /**
     * 获取recyclerView选中的索引
     * @param position
     */
    @Override
    public void onItemClick(int position) {
        setBackgroundIndex(position);
        fromSharedPreferencesGetBgIndex(recyclerView);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        fromSharedPreferencesGetBgIndex(recyclerView);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        list.clear();
    }

    /**
     * 初始化图片列表
     */
    private void initImageList(){
        int[] images = ImageResourceUtil.getImages();
        for(int item:images){
            DressUpData dressUpData = new DressUpData();
            dressUpData.setImagePath(item);
            dressUpData.setHeight(new Random().nextInt(200)+400);
            list.add(dressUpData);
        }
    }

    /**
     * 设置view中每一项的间距
     */
    private class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        public SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view,
                                   RecyclerView parent, RecyclerView.State state) {
            outRect.left = space - 18;
            outRect.right = space - 18;
            outRect.bottom = space ;

            // Add top margin only for the first item to avoid double space between items
            if (parent.getChildPosition(view) == 0)
                outRect.top = space;
        }
    }
}
