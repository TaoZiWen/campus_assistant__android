package com.example.schoolassistant_android.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.viewpager.widget.ViewPager;

import com.example.schoolassistant_android.R;
import com.example.schoolassistant_android.Adapter.MyFragmentPagerAdapter;
import com.example.schoolassistant_android.base.BaseActivity;
import com.google.android.material.navigation.NavigationView;

import butterknife.Bind;
import butterknife.ButterKnife;

public class HomeActivity extends BaseActivity implements RadioGroup.OnCheckedChangeListener,
        ViewPager.OnPageChangeListener, NavigationView.OnNavigationItemSelectedListener {

    @Bind(R.id.nav_view)
    NavigationView navigationView;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.rg_tab_bar)
    RadioGroup rg_tab_bar;
    @Bind(R.id.rb_school)
    RadioButton rb_school;
    @Bind(R.id.rb_my)
    RadioButton rb_my;
    @Bind(R.id.vpager)
    ViewPager vpager;
    @Bind(R.id.drawer_layout)
    DrawerLayout drawer;

    private MyFragmentPagerAdapter mAdapter;

    //几个代表页面的常量
    public static final int PAGE_ONE = 0;
    public static final int PAGE_TWO = 1;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        initData();
        initView();
        initListener();
    }

    @Override
    protected void initData() {
        ButterKnife.bind(this);
        mAdapter = new MyFragmentPagerAdapter(getSupportFragmentManager());
    }

    @Override
    protected void initView() {
        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.nav_open, R.string.nav_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        vpager.setAdapter(mAdapter);
        vpager.setCurrentItem(0);

        rb_school.setChecked(true);
    }

    @Override
    protected void initListener() {
        navigationView.setNavigationItemSelectedListener(this);

        rg_tab_bar.setOnCheckedChangeListener(this);

        vpager.addOnPageChangeListener(this);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.openDrawer(navigationView);
            }
        });
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.rb_school:
                vpager.setCurrentItem(PAGE_ONE);
                break;
            case R.id.rb_my:
                vpager.setCurrentItem(PAGE_TWO);
                break;
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {
        //state的状态有三个，0表示什么都没做，1正在滑动，2滑动完毕
        if (state == 2) {
            switch (vpager.getCurrentItem()) {

                case PAGE_ONE:
                    rb_school.setChecked(true);
                    break;
                case PAGE_TWO:
                    rb_my.setChecked(true);
                    break;
            }
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_home:
                Log.i("navigationView", "Home is clicked!");
                break;
            case R.id.nav_gallery:
                Log.i("navigationView", "Gallery is clicked!");
                break;
            case R.id.nav_slideshow:
                changeUI(DressUpActivity.class);
                break;
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        fromSharedPreferencesGetBgIndex(drawer, navigationView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_reserver:
                setBackgroundIndex(-1,drawer, navigationView);
                showToast("恢复默认风格成功!!正在重启app",1);
                restartApplication();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
