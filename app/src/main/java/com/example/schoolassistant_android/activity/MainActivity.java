package com.example.schoolassistant_android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;

import com.example.schoolassistant_android.R;
import com.example.schoolassistant_android.base.BaseActivity;
import com.example.schoolassistant_android.constant.Config;
import com.example.schoolassistant_android.net.RequestView;
import com.example.schoolassistant_android.utils.MMKVUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity {
    @Bind(R.id.welcome_layout)
    RelativeLayout welcomeLayout;

    private static final String TAG = "MainActivity:";

    private Timer timer;
    private TimerTask task;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 判断是否是第一次开启应用
        boolean isFirstOpen = (Boolean) MMKVUtils.getInstance().get(Config.FIRST_OPEN, false);
        // 如果是第一次启动，则先进入功能引导页
        if (!isFirstOpen) {
            Intent intent = new Intent(this, WelcomePageActivity.class);
            startActivity(intent);
            finish();
        }
        // 如果不是第一次启动app，则正常显示启动屏
        setContentView(R.layout.activity_main);
        initData();
        initView();
        initListener();
    }

    @Override
    protected void initData() {
        ButterKnife.bind(this);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void initView() {
        runTimer();
    }

    @Override
    protected void initListener() {

    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            return true;
        } else {
            return super.dispatchKeyEvent(event);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(String str) {
        Log.d(TAG, "onEvent:" + str);
        Intent intent = null;
        Boolean mIsLogin = (Boolean) MMKVUtils.getInstance().get(Config.IS_LOGIN, false);
        if (!mIsLogin) {
            intent = new Intent(this, LoginActivity.class);
        }else{
            intent = new Intent(this, HomeActivity.class);
        }
        startActivity(intent);
        finish();
    }

    /**
     * 定时加载图片
     */
    public void runTimer() {
        timer = new Timer();
        task = new TimerTask() {
            @Override
            public void run() {
                EventBus.getDefault().post("1");
            }
        };
        timer.schedule(task, 2000);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
