package com.example.schoolassistant_android.activity;

import androidx.annotation.Nullable;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.example.schoolassistant_android.R;
import com.example.schoolassistant_android.base.BaseActivity;
import com.example.schoolassistant_android.domain.LoginData;
import com.example.schoolassistant_android.net.RequestView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.Bind;
import butterknife.ButterKnife;

public class LoginActivity extends BaseActivity implements RequestView.LoginView {

    @Bind(R.id.iv_qq)
    ImageView iv_qq;
    @Bind(R.id.btn_login)
    Button btnLogin;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initData();
        initView();
        initListener();
    }

    @Override
    protected void initData() {
        ButterKnife.bind(this);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initListener() {
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeUI(HomeActivity.class);
                finish();
            }
        });
    }

    @Override
    public void success(LoginData loginData) {

    }

    @Override
    public void failed(Throwable code) {

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(String str) {

    }
}
