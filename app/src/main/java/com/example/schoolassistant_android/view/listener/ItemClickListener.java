package com.example.schoolassistant_android.view.listener;

/**
 * 所属包：com.example.schoolassistant_android.view.listener
 * 作者：Administrator on 2020/8/25 14:10
 * 邮箱：tzw1109296630@163.com
 * recycleview的点击事事件
 */
public interface ItemClickListener {

    public void onItemClick(int position) ;
}
