package com.example.schoolassistant_android;

import android.app.Application;
import android.content.Context;

import com.example.schoolassistant_android.utils.MMKVUtils;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.tencent.mmkv.MMKV;

/**
 * 所属包：com.example.schoolassistant_android
 * 作者：Administrator on 2020/8/18 10:27
 * 邮箱：tzw1109296630@163.com
 */
public class LauncherApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        //基于 mmap 内存映射的移动端通用 key-value 组件，底层序列化/反序列化使用 protobuf 实现，性能高，稳定性强
        MMKV.initialize(this);
        //Fresco图片加载组件
        //将图片放到一个特别的内存区域。当然，在图片不显示的时候，占用的内存会自动被释放。
        // 这会使得APP更加流畅，减少因图片内存占用而引发的OOM。
        Fresco.initialize(this);
        //初始化key-value 组件工具类
        MMKVUtils.getInstance();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    // 程序在内存清理的时候执行（回收内存）
    // HOME键退出应用程序、长按MENU键，打开Recent TASK都会执行
    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
