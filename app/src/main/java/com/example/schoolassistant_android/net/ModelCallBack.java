package com.example.schoolassistant_android.net;

import com.example.schoolassistant_android.domain.LoginData;

/**
 * 所属包：com.example.schoolassistant_android.net
 * 作者：Administrator on 2020/8/18 15:00
 * 邮箱：tzw1109296630@163.com
 * 执行请求时的回调方法
 */
public interface ModelCallBack {
    interface LoginCallBack {
        void success(LoginData loginData);

        void failed(Throwable code);
    }
}
