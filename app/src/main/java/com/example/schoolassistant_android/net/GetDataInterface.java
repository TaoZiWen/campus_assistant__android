package com.example.schoolassistant_android.net;

import com.example.schoolassistant_android.domain.LoginData;
import com.example.schoolassistant_android.domain.request.RequestLoginData;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * 所属包：com.example.schoolassistant_android.net
 * 作者：Administrator on 2020/8/18 15:12
 * 邮箱：tzw1109296630@163.com
 */
public interface GetDataInterface {
    //登录的接口
    @POST("")
    Observable<LoginData> login(@Body RequestLoginData requestLoginData);
}
