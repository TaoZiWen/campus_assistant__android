package com.example.schoolassistant_android.net;

import com.example.schoolassistant_android.domain.LoginData;

/**
 * 所属包：com.example.schoolassistant_android.net
 * 作者：Administrator on 2020/8/18 13:52
 * 邮箱：tzw1109296630@163.com
 */
public class RequestView {
    //登录页面由presenter层交互数据到view层MainActivity
    public interface LoginView {
        //登录时，数据获取成功的方法，返回一个值表示登陆成功
        void success(LoginData loginData);

        //登录时，数据获取失败的方法，返回一个int值响应码表示登陆失败
        void failed(Throwable code);
    }
//
//    //注册页面由presenter层到view层RegActivity
//    public interface RegView {
//
//        void failed(Throwable code);
//
//        void success(RegBean regBean);
//
//        void getVerify(VerifyBean verifyBean);
//
//    }
//
//    public interface ResetPwdView{
//
//        void failed(Throwable code);
//
//        void resetPwd(ResetPwdBean resetPwdBean);
//
//        void getVerify(VerifyBean verifyBean);
//    }
}
