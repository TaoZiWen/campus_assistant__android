package com.example.schoolassistant_android.net.presenter;

import android.util.Log;

import com.example.schoolassistant_android.domain.LoginData;
import com.example.schoolassistant_android.net.ModelCallBack;
import com.example.schoolassistant_android.net.RequestView;
import com.example.schoolassistant_android.net.model.LoginModel;

/**
 * 所属包：com.example.schoolassistant_android.net.presenter
 * 作者：Administrator on 2020/8/18 14:57
 * 邮箱：tzw1109296630@163.com
 */
public class LoginPresenter {
    private RequestView.LoginView loginView;
    private LoginModel loginModel = new LoginModel();

    public LoginPresenter(RequestView.LoginView loginView) {
        this.loginView = loginView;
    }
    public void getData(String tel, String pwd, String userType, String uuid) {
        loginModel.getLoginData(tel, pwd, userType, uuid, new ModelCallBack.LoginCallBack() {
            @Override
            public void success(LoginData loginData) {
                Log.d("LoginPresenter", "loginBean:" + loginData);
                loginView.success(loginData);

            }

            @Override
            public void failed(Throwable code) {
                loginView.failed(code);
            }
        });
    }
}
