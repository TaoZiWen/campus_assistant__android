package com.example.schoolassistant_android.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.example.schoolassistant_android.R;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PersonFragment extends Fragment {

    private View view;
    private ListView listView;
    private List<Map<String, Object>> listitem;
    private SimpleAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_my, container, false);
        initview();
        return view;
    }

    private void initview() {
        listitem = new ArrayList<>();
        int[] img_list = new int[]{R.mipmap.star, R.mipmap.chart, R.mipmap.location};
        String[] menu_item = new String[]{"收藏", "图表", "地址"};

        for (int i = 0; i < img_list.length; i++) {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("img_list", img_list[i]);
            map.put("menu_item", menu_item[i]);
            listitem.add(map);
        }
        adapter = new SimpleAdapter(getContext()
                , listitem
                , R.layout.list_item_view
                , new String[]{"img_list", "menu_item"}
                , new int[]{R.id.img_icon, R.id.textcontent});
        listView = view.findViewById(R.id.listviews);
        listView.setAdapter(adapter);
    }

}
