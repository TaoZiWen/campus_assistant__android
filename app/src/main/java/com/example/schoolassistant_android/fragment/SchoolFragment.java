package com.example.schoolassistant_android.fragment;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.example.schoolassistant_android.Adapter.MyGridViewAdapter;
import com.example.schoolassistant_android.R;
import com.example.schoolassistant_android.utils.MyGridView;
import com.youth.banner.Banner;
import com.youth.banner.BannerConfig;
import com.youth.banner.Transformer;
import com.youth.banner.listener.OnBannerListener;
import com.youth.banner.loader.ImageLoader;

import java.util.ArrayList;

public class SchoolFragment extends Fragment implements OnBannerListener {
    private static final String TAG = "SchoolFragment:";

    private Banner banner;
    private ArrayList<Integer> ban_path;
    private ArrayList<String> ban_title;
    private View view;
    private MyGridView gridView;

    private int[] mImgArr = {R.mipmap.a,R.mipmap.b,R.mipmap.c,R.mipmap.d,R.mipmap.f,R.mipmap.g,R.mipmap.h,R.mipmap.i};
    private String[] mImgTitle = {"校门","活动","晚会","升旗","走廊","教学楼","风景","学校"};

    public SchoolFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_school, container, false);
        bannerview();
        initview();
        return view;
    }

    private void initview() {
        gridView = view.findViewById(R.id.main_gridview);
        gridView.setAdapter(new MyGridViewAdapter(getActivity()));
    }

    private void bannerview() {
        banner = view.findViewById(R.id.banner);
        ban_path = new ArrayList<>();
        ban_title = new ArrayList<>();
        for(int i=0;i<mImgArr.length;i++){
            ban_path.add(mImgArr[i]);
            ban_title.add(mImgTitle[i]);
        }
        banner.setBannerStyle(BannerConfig.CIRCLE_INDICATOR_TITLE_INSIDE);
        banner.setImageLoader(new MyLoad());
        //设置图片网址或地址的集合
        banner.setImages(ban_path);
        //设置轮播的动画效果，内含多种特效，可点入方法内查找后内逐一体验
        banner.setBannerAnimation(Transformer.Default);
        //设置轮播图的标题集合
        banner.setBannerTitles(ban_title);
        //设置轮播间隔时间
        banner.setDelayTime(3000);
        //设置是否为自动轮播，默认是“是”
        banner.isAutoPlay(true);
        //设置指示器的位置，小点点，左中右。
        banner.setIndicatorGravity(BannerConfig.CENTER).setOnBannerListener(this).start();

    }

    @Override
    public void OnBannerClick(int position) {
        Log.i("tag", "你点了第" + position + "张轮播图");
    }


    public class MyLoad extends ImageLoader {
        @Override
        public void displayImage(Context context, Object path, ImageView imageView) {
            Glide.with(context.getApplicationContext()).load(path).into(imageView);
        }
    }

}
