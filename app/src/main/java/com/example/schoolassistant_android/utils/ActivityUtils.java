package com.example.schoolassistant_android.utils;

import android.app.Activity;
import android.util.Log;

import java.util.LinkedList;
import java.util.List;

/**
 * 所属包：com.example.schoolassistant_android.utils
 * 作者：Administrator on 2020/8/18 10:50
 * 邮箱：tzw1109296630@163.com
 * Activity管理工具类
 */
public class ActivityUtils {
    private List<Activity> activityList = new LinkedList<Activity>();
    private static ActivityUtils instance;

    private ActivityUtils() {
    }

    // 单例模式中获取唯一的ExitAPPUtils实例
    public static ActivityUtils getInstance() {
        if (null == instance) {
            instance = new ActivityUtils();
        }
        return instance;
    }

    // 添加Activity到容器中
    public void addActivity(Activity activity) {
        activityList.add(activity);
    }

    public Activity getActivity() {
        Activity ac = null;
        if (activityList.size() > 0) {
            ac = activityList.get(activityList.size() - 1);
        }
        if (ac == null) {
            ac = new Activity();
        }

        return ac;
    }

    // 遍历所有Activity并finish

    public void exit() {
        for (Activity activity : activityList) {
            activity.finish();
        }
        activityList.clear();
    }

    /**
     * 关闭指定的Activity
     * @param activityName
     */
    public void exitNow(String activityName) {
        for (Activity activity : activityList) {
            if (activity.getClass().getSimpleName().equals(activityName)) {
                Log.d("ActivityUtils", activity.getClass().getSimpleName());
                activity.finish();
                break;
            }
        }
    }
}
