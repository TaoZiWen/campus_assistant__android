package com.example.schoolassistant_android.utils;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.util.Log;


import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;
/**
 * 所属包：com.example.schoolassistant_android.utils
 * 作者：Administrator on 2020/8/18 10:30
 * 邮箱：tzw1109296630@163.com
 * 权限申请获取类
 */
public class PermissionManager {

    private static final String TAG = "PermissionManager";


    /**
     * android 6.0 以上需要动态申请权限
     */
    public static  void initPermission(Activity activity) {
        String[] permissions = {
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_NETWORK_STATE,
                Manifest.permission.INTERNET,
                Manifest.permission.READ_EXTERNAL_STORAGE,

        };

        ArrayList<String> toApplyList = new ArrayList<String>();

        for (String perm : permissions) {
            if (PackageManager.PERMISSION_GRANTED != ContextCompat.checkSelfPermission(activity, perm)) {
                toApplyList.add(perm);
                // 进入到这里代表没有权限.
                Log.i(TAG,"没有权限");

            }
        }
        String[] tmpList = new String[toApplyList.size()];
        if (!toApplyList.isEmpty()) {
            ActivityCompat.requestPermissions(activity, toApplyList.toArray(tmpList), 123);
        }


    }



}
