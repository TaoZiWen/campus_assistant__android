package com.example.schoolassistant_android.utils;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
/**
 * 所属包：com.example.schoolassistant_android.utils
 * 作者：Administrator on 2020/8/18 14:58
 * 邮箱：tzw1109296630@163.com
 *
 */
public class RetrofitUtil {
    private Retrofit mRetrofit;
    private String baseUrl;
    OkHttpClient client;
    private static RetrofitUtil mRetrofitManager;
    private RetrofitUtil(String baseUrl, OkHttpClient client){
        this.baseUrl=baseUrl;
        this.client=client;
        initRetrofit();
    }

    //单例模式+双重锁模式 封装方法
    public static synchronized RetrofitUtil getInstance(String baseUrl, OkHttpClient client){
        if (mRetrofitManager == null){
            mRetrofitManager = new RetrofitUtil(baseUrl,client);
        }
        return mRetrofitManager;
    }

    //实例化Retrofit请求
    private void initRetrofit() {
        mRetrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build();

    }

    //封装泛型方法
    public <T> T setCreate(Class<T> reqServer) {
        return mRetrofit.create(reqServer);
    }
}
