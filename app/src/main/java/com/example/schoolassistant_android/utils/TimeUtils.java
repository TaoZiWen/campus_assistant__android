package com.example.schoolassistant_android.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 所属包：com.example.schoolassistant_android.utils
 * 作者：Administrator on 2020/8/18 10:46
 * 邮箱：tzw1109296630@163.com
 * 时间工具类
 */
public class TimeUtils {

    public static String getTime() {
        SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = sDateFormat.format(new Date());
        return date;
    }
    /**
     * 获取yyyy-MM-dd 格式的当前日期
     **/
    public static String getYearAndMonthAndDayTime() {
        SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyy-MM-dd");

        String date = sDateFormat.format(new Date());
        return date;
    }

    /**
     * 获取hh:mm 格式的当前日期
     **/
    public static String getMinuteAndSecondTime() {
        SimpleDateFormat mSimpleDate = new SimpleDateFormat("HH:mm");
        if (mSimpleDate == null) {
            mSimpleDate = new SimpleDateFormat("HH:mm");
        }
        String date = mSimpleDate.format(new Date());
        String[] strs = date.split(" ");
        if (strs.length == 2) {
            return strs[1];
        }
        return date;
    }
}
