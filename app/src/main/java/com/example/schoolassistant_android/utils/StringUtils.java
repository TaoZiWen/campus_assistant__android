package com.example.schoolassistant_android.utils;

import android.widget.EditText;
import android.widget.ImageView;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {

    public static void getCode(ImageView verificationImage) {
        verificationImage.setImageBitmap(VerifyCode.getInstance().createBitmap());
    }


    /**
     * 检测手机号码，邮箱等是否有效
     * Created by xiaolijuan on 2016/8/21.
     */

    /**
     * 要更加准确的匹配手机号码只匹配11位数字是不够的，比如说就没有以144开始的号码段，
     * 故先要整清楚现在已经开放了多少个号码段，国家号码段分配如下：
     * 移动：134、135、136、137、138、139、150、151、157(TD)、158、159、187、188
     * 联通：130、131、132、152、155、156、185、186 　　电信：133、153、180、189、（1349卫通）
     */
    public static boolean isPhone(String phone) {
        String regex = "^((13[0-9])|(14[5,7,9])|(15([0-3]|[5-9]))|(166)|(17[0,1,3,5,6,7,8])|(18[0-9])|(19[8|9]))\\d{8}$";
        if (phone.length() != 11) {
            return false;
        } else {
            return phone.matches(regex);
        }
    }

    /**
     * 判断email格式是否正确
     */
    public static boolean isEmail(String email) {
        String str = "^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$";
        Pattern p = Pattern.compile(str);
        Matcher m = p.matcher(email);
        return m.matches();
    }

    /**
     * 检测密码是否正确
     *
     * @param passwordEdit
     * @return
     */
    public static boolean checkPassword(EditText passwordEdit) {
        int len = getLen(passwordEdit);
        if (!(len < 6) && !(len > 16)) {
            return true;
        }
        return false;

    }


    /**
     * 检测账号是否正确
     *
     * @param accountEdit
     * @return
     */
    public static boolean checkAccount(EditText accountEdit) {
        int len = getLen(accountEdit);
        if (!(len < 3) && !(len > 12) && accountEdit.getText().toString().trim().matches("^[a-zA-Z_][0-9a-zA-Z_]+$")) {
            return true;
        }
        return false;

    }

    /**
     * 检测学号是否正确
     *
     * @param stuIdEdit
     * @return
     */
    public static boolean checkStuId(EditText stuIdEdit) {
        int len = getLen(stuIdEdit);
        if ((len == 11) && stuIdEdit.getText().toString().trim().matches("^[2][0][0-9][0-9][0-9]{7}$")) {
            return true;
        }
        return false;

    }

    /**
     * 获取字符的长度
     *
     * @param edit 输入框
     * @return
     */
    private static int getLen(EditText edit) {
        return edit.getText().toString().trim().length();
    }

    public static boolean checkVerifyCode(EditText verifyCodeEdit) {
        if (getLen(verifyCodeEdit) == 4) {
            return true;
        }
        return false;

    }

    /**
     * 判断输入的内容是否为空
     *
     * @param str
     * @return
     */
    public static boolean isBlank(String str) {
        if (str == null || str.equals("") || str.length() < 1) {
            return true;
        }
        return false;
    }
    /**
     * @Description 判断字符串是否有值，如果为null或者是空字符串或者只有空格或者为"null"字符串，则返回true，否则则返回false
     */
    public static boolean isEmpty(String value) {
        if (value != null && !"".equalsIgnoreCase(value.trim()) && !"null".equalsIgnoreCase(value.trim())) {
            return false;
        } else {
            return true;
        }
    }
    /**
     * @Description 保持小数点后两位
     */
    public static String formatDoublePointTwo(double money) {
        DecimalFormat formater = new DecimalFormat();
        formater.setMaximumFractionDigits(2);
        formater.setGroupingSize(0);
        formater.setRoundingMode(RoundingMode.FLOOR);
        return formater.format(money);
    }

    public static boolean hasString(String msg) {
        if (msg == null) {
            return false;
        }

        if (msg.length() < 1) {
            return false;
        }
        return true;
    }
}

