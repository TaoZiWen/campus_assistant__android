package com.example.schoolassistant_android.utils;


import com.example.schoolassistant_android.R;

/**
 * @author 安缇吖
 * 工具类:存放图片资源
 * @package com.example.schoolassistant_android.utils
 * @date 2018/11/9 0009  16:24
 * @email 1972401425@qq.com
 */
public class ImageResourceUtil {
    private ImageResourceUtil() {
    }

    /**
     * 初始化图片资源
     *
     * @return
     */

    public static int[] IMAGES = {

            R.mipmap.b01, R.mipmap.b02, R.mipmap.b03, R.mipmap.b04, R.mipmap.b05,
            R.mipmap.b06, R.mipmap.b07, R.mipmap.b08, R.mipmap.b09, R.mipmap.b10,
            R.mipmap.b11
    };

    public static int[] getImages() {
        return IMAGES;
    }
}
