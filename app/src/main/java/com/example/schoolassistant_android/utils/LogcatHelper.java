package com.example.schoolassistant_android.utils;

import android.content.Context;
import android.os.Environment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 所属包：com.example.schoolassistant_android.utils
 * 作者：Administrator on 2020/8/18 10:45
 * 邮箱：tzw1109296630@163.com
 * log日志统计保存
 */
public class LogcatHelper {
    private static LogcatHelper INSTANCE = null;
    private static String PATH_LOGCAT;
    private boolean isRun = false;
    private LogDumper mLogDumper = null;
    private int mPId;
    private StringBuffer text = new StringBuffer();
    private Context context;

    private boolean logSwitch = false;

    /**
     * 初始化目录
     */
    public void init(Context context) {
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {// 优先保存到SD卡中
            PATH_LOGCAT = Environment.getExternalStorageDirectory()
                    .getAbsolutePath() + File.separator + "desktop-Logs";
        } else {// 如果SD卡不存在，就保存到本应用的目录下
            PATH_LOGCAT = context.getFilesDir().getAbsolutePath()
                    + File.separator + "desktop-Logs";
        }
        File file = new File(PATH_LOGCAT);
        if (!file.exists()) {
            file.mkdirs();
        }
    }

    public static LogcatHelper getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new LogcatHelper(context);
        }
        return INSTANCE;
    }
//    public static LogcatHelper getInstance() {
//        return INSTANCE;
//    }

    public synchronized void setLine(String str) {

        if (!logSwitch) {
            if (isRun) {
                stop();
                isRun = false;
            }

            return;
        }
        if (mLogDumper == null) {
            if (text.length() > 1) {
                text.append(TimeUtils.getTime() + str + "\n");
            } else {
                text.append(str + "\n");
            }
            start();
            return;
        }
        if (text.length() > 1) {
            str += (text.toString() + TimeUtils.getTime() + str);
            text = new StringBuffer();
        }
        mLogDumper.setLine(":" + str + "\n");
    }

    private LogcatHelper(Context context) {
        init(context);
        this.context = context;
        mPId = android.os.Process.myPid();
    }

    public void start() {
        if (!logSwitch) {
            return;
        }
        if (mLogDumper == null)
            mPId = android.os.Process.myPid();
        mLogDumper = new LogDumper(String.valueOf(mPId), PATH_LOGCAT);

        if (!isRun) {
            mLogDumper.start();
            isRun = true;
        }
    }

    public void stop() {
        if (mLogDumper != null) {
            mLogDumper.stopLogs();
            mLogDumper = null;
        }
        isRun = false;
    }


    private class LogDumper extends Thread {

        private Process logcatProc;
        private BufferedReader mReader = null;
        private boolean mRunning = true;
        private String line = null;
        String cmds = null;
        private String mPID;
        private FileOutputStream out = null;
        private File file;

        public LogDumper(String pid, String dir) {
            mPID = pid;
            file = new File(dir, ""
                    + MyDate.getFileName() + ".txt");
            try {

                out = new FileOutputStream(file, true);

            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            /**
             *
             * 日志等级：*:v , *:d , *:w , *:e , *:f , *:s
             *
             * 显示当前mPID程序的 E和W等级的日志.
             *
             * */

            // cmds = "logcat *:e *:w | grep \"(" + mPID + ")\"";
            // cmds = "logcat  | grep \"(" + mPID + ")\"";//打印所有日志信息
            // cmds = "logcat -s way";//打印标签过滤信息
            cmds = "logcat *:e *:i | grep \"(" + mPID + ")\"";

        }

        public void stopLogs() {
            mRunning = false;
        }

        public void setLine(String str) {
            if (line != null) {
                str += TimeUtils.getTime() + str;
                this.line += "\n" + str;

            } else {
                this.line = str;
            }
        }

        @Override
        public void run() {
            try {
                logcatProc = Runtime.getRuntime().exec(cmds);
                mReader = new BufferedReader(new InputStreamReader(
                        logcatProc.getInputStream()), 1024);

                while (mRunning) {
                    if (!mRunning) {
                        break;
                    }
                    if (!file.exists()) {
                        out = new FileOutputStream(file, true);
                    }
                    if (out != null && line != null) {
                        out.write((MyDate.getDateEN() + "  " + line + "\n")
                                .getBytes());
                        line = null;
                    }
                }

            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (logcatProc != null) {
                    logcatProc.destroy();
                    logcatProc = null;
                }
                if (mReader != null) {
                    try {
                        mReader.close();
                        mReader = null;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (out != null) {
                    try {
                        out.close();
                    } catch (IOException e) {
                        e.printStackTrace();

                    }
                    out = null;
                }

            }

        }

    }

    public static class MyDate {
        public static String getFileName() {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            String date = format.format(new Date(System.currentTimeMillis()));
            return date;// 2012年10月03日 23:41:31
        }

        public static String getDateEN() {
            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String date1 = format1.format(new Date(System.currentTimeMillis()));
            return date1;// 2012-10-03 23:41:31
        }

    }
}
