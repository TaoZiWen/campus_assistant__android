package com.example.schoolassistant_android.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.schoolassistant_android.R;
import com.example.schoolassistant_android.utils.RoundTransform;
import com.squareup.picasso.Picasso;


public class MyGridViewAdapter extends BaseAdapter {

    private Context context;

    //12星座图片
    private int[] imgs = {R.mipmap.class_schedule, R.mipmap.score, R.mipmap.water, R.mipmap.car,
            R.mipmap.sign_in, R.mipmap.expressage
    };


    //12星座文字
    private String[] strs = {"课程表", "成绩", "水电费",
            "饭卡", "打卡", "快递查询"
    };


    public MyGridViewAdapter(Context context) {
        super();
        this.context = context;
    }

    @Override
    public int getCount() {
        return imgs.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.main_item, parent, false);
            holder = new ViewHolder();
            holder.imageView = (ImageView) convertView.findViewById(R.id.item_img);
            holder.textView = (TextView) convertView.findViewById(R.id.item_txt);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("onClickListener:", holder.textView.getText().toString());
            }
        };
        //holder.imageView.setBackgroundResource(imgs[position]);
        Picasso.with(context).load(imgs[position]).transform(new RoundTransform()).into(holder.imageView);
        holder.textView.setText(strs[position]);
        holder.imageView.setOnClickListener(onClickListener);
        holder.textView.setOnClickListener(onClickListener);

        return convertView;
    }

    class ViewHolder {
        ImageView imageView;
        TextView textView;
    }
}