package com.example.schoolassistant_android.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.schoolassistant_android.R;
import com.example.schoolassistant_android.domain.DressUpData;
import com.example.schoolassistant_android.view.listener.ItemClickListener;

import java.util.List;

/**
 * 所属包：com.example.schoolassistant_android.Adapter
 * 作者：Administrator on 2020/8/25 12:00
 * 邮箱：tzw1109296630@163.com
 */
public class DressUpAdapter extends RecyclerView.Adapter<DressUpAdapter.MyViewHolder> {

    private List<DressUpData> list;
    private Context context;

    // 利用接口 -> 给RecyclerView设置点击事件
    private ItemClickListener mItemClickListener ;

    public void setOnItemClickListener(ItemClickListener itemClickListener){
        this.mItemClickListener = itemClickListener ;

    }
    public DressUpAdapter(Context context,List<DressUpData> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        DressUpData dressUpData = list.get(position);

        ViewGroup.LayoutParams lp = holder.mImageView.getLayoutParams();
        lp.height = dressUpData.getHeight();

        holder.mImageView.setLayoutParams(lp);

        holder.mImageView.setImageDrawable(context.getResources().getDrawable(dressUpData.getImagePath()));

        if(mItemClickListener != null){
            holder.mImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mItemClickListener.onItemClick(position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView mImageView; // 标签

        public MyViewHolder(View itemView) {
            super(itemView);
            mImageView = (ImageView) itemView.findViewById(R.id.img_recyclerView_item);
        }
    }
}
