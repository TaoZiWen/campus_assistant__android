package com.example.schoolassistant_android.Adapter;

import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.schoolassistant_android.activity.HomeActivity;
import com.example.schoolassistant_android.fragment.SchoolFragment;
import com.example.schoolassistant_android.fragment.PersonFragment;


public class MyFragmentPagerAdapter extends FragmentPagerAdapter {

    private final int PAGER_COUNT = 2;
    private SchoolFragment schoolFragment = null;
    private PersonFragment myFragment2 = null;


    public MyFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
        schoolFragment = new SchoolFragment();
        myFragment2 = new PersonFragment();
    }


    @Override
    public int getCount() {
        return PAGER_COUNT;
    }

    @Override
    public Object instantiateItem(ViewGroup vg, int position) {
        return super.instantiateItem(vg, position);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case HomeActivity.PAGE_ONE:
                fragment = schoolFragment;
                break;
            case HomeActivity.PAGE_TWO:
                fragment = myFragment2;
                break;
        }
        return fragment;
    }


}

