package com.example.schoolassistant_android.domain.request;

/**
 * 所属包：com.example.schoolassistant_android.domain.request
 * 作者：Administrator on 2020/8/18 15:19
 * 邮箱：tzw1109296630@163.com
 */
public class RequestLoginData {
    public String passWord;
    public String sign;
    public String uuid;
    public String userName;

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        return "RequestLoginData{" +
                "passWord='" + passWord + '\'' +
                ", sign='" + sign + '\'' +
                ", uuid='" + uuid + '\'' +
                ", userName='" + userName + '\'' +
                '}';
    }
}
