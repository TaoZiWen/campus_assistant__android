package com.example.schoolassistant_android.domain;

import android.os.Parcel;

import com.example.schoolassistant_android.base.BaseData;

/**
 * 所属包：com.example.schoolassistant_android.domain
 * 作者：Administrator on 2020/8/18 14:46
 * 邮箱：tzw1109296630@163.com
 */
public class LoginData extends BaseData {
    private BodyBean body;

    public static class BodyBean{
        private int age;
        private String email;
        private String nickName;
        private String passWord;
        private String phoneNo;
        private int sex;
        private String userHeadPicUrl;
        private int userId;
        private int userType;
        private String token;
        private String remark;
        private String address;
        private String school;
        private String gradle;
        private String mClass;
    }
}
