package com.example.schoolassistant_android.domain;

/**
 * 所属包：com.example.schoolassistant_android.domain
 * 作者：Administrator on 2020/8/25 11:51
 * 邮箱：tzw1109296630@163.com
 */
public class DressUpData {
    private int imagePath;

    private int height;

    public int getImagePath() {
        return imagePath;
    }

    public void setImagePath(int imagePath) {
        this.imagePath = imagePath;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return "DressUpData{" +
                "imagePath=" + imagePath +
                ", height=" + height +
                '}';
    }
}
