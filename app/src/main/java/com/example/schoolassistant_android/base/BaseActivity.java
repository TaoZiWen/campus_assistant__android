package com.example.schoolassistant_android.base;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.schoolassistant_android.constant.Config;
import com.example.schoolassistant_android.utils.ActivityUtils;
import com.example.schoolassistant_android.utils.ImageResourceUtil;
import com.example.schoolassistant_android.utils.LogcatHelper;
import com.example.schoolassistant_android.utils.MMKVUtils;
import com.example.schoolassistant_android.utils.PermissionManager;
import com.example.schoolassistant_android.utils.SharedPreferencesUtils;
import com.example.schoolassistant_android.utils.StringUtils;

/**
 * 所属包：com.example.schoolassistant_android.activity.base
 * 作者：Administrator on 2020/8/18 10:30
 * 邮箱：tzw1109296630@163.com
 */
public abstract class BaseActivity extends AppCompatActivity {

    private boolean exitFlag;
    protected int[] images;
    private ProgressDialog pd = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //初始化应用相关权限
        PermissionManager.initPermission(this);
        //初始化日志统计
        LogcatHelper.getInstance(this);
        //初始化Activity管理工具
        ActivityUtils.getInstance().addActivity(this);
        //初始化属性
        this.initProperty();
    }

    /**
     * 初始化属性
     */
    private void initProperty() {
        //daoImpl = UserDaoImpl.getInstance();
        images = ImageResourceUtil.getImages();
    }

    /**
     * 获取一张验证码
     *
     * @param imgVerifyImage 加载图片的控件
     */
    protected void changeVerifyCodeImage(ImageView imgVerifyImage) {
        StringUtils.getCode(imgVerifyImage);
    }

    /**
     * 线程休眠
     *
     * @param time
     */
    protected void threadSleep(long time) {
        SystemClock.sleep(time);
    }

    /**
     * 初始化变量及数据值
     */
    protected abstract void initData();

    /**
     * 初始化控件的方法
     */
    protected abstract void initView();

    /**
     * 实现控件的监听事件
     */
    protected abstract void initListener();

    /**
     * 吐司信息
     */
    protected void showToast(String message,int time) {
        Toast.makeText(getApplicationContext(), message, time==0?Toast.LENGTH_SHORT:Toast.LENGTH_LONG).show();
    }

    /**
     * 跳转新得页面
     *
     * @param newActivity 新的activity
     */
    protected void changeUI(Class newActivity) {
        startActivity(new Intent(getApplicationContext(), newActivity));
    }

    /**
     * 跳转网页
     *
     * @param url
     */
    protected void startToInternet(String url) {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
    }

    /**
     * 跳转传递值
     *
     * @param newActivity 新的activity
     * @param intent
     */
    protected void changeUiWithText(Class newActivity, Intent intent) {
        intent.setClass(getApplicationContext(), newActivity);
        startActivity(intent);
    }

//    /**
//     * 切换fragment
//     *
//     * @param ViewId   要加载fragment的控件
//     * @param fragment 要加载显示的fragment界面
//     */
//    protected void changeFragment(int ViewId, Fragment fragment) {
//        getSupportFragmentManager().beginTransaction().replace(ViewId, fragment).commit();
//    }
//
//    protected void changeFragmentWithText(int ViewId, Fragment fragment) {
//        getSupportFragmentManager().beginTransaction().replace(ViewId, fragment).commit();
//    }



    /**
     * qq联系作者
     */
    protected void contactTheAuthor(String qq) {
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("mqqwpa://im/chat?chat_type=wpa&uin=" + qq + "&version=1")));
        }
        catch (Exception e) {
            e.printStackTrace();
            showToast("请安装QQ客户端进行联系",0);
        }
    }





    /**
     * 弹出加载对话框
     */
    protected void showLoadDialog(Context context,String message) {
        if(pd==null) {
            pd = new ProgressDialog(context);
            pd.setCancelable(true);
            pd.setMessage(message);
            pd.show();
        }
    }
    /**
     * 关闭加载对话框
     */
    protected void closeLoadDialog() {
        if(pd!=null) {
            pd.dismiss();
            pd = null;
        }
    }

    /**
     * 设置背景
     *
     * @param position
     * @param views
     */
    protected void setBackgroundIndex(int position, View... views) {
        MMKVUtils.getInstance().put(Config.BACKGROUND_STYLE,position);
        for (View view : views) {
            if(position!=-1) {
                view.setBackgroundResource(images[position]);
            }
        }
    }

    /**
     * 从偏好文件设置获取自己设置的皮肤
     */
    protected void fromSharedPreferencesGetBgIndex(View... views) {
        int position = (int)MMKVUtils.getInstance().get(Config.BACKGROUND_STYLE,-1);
        if (views != null&&position!=-1) {
            for (View view : views) {
                view.setBackgroundResource(images[position]);
            }
        }
    }
    //重启应用（应用重新启动自身）
    public void restartApplication() {
        final Intent intent = getPackageManager().getLaunchIntentForPackage(getPackageName());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        int i = 1 / 0;//借助异常关闭当前已经开启的app
    }

    /**
     * 双击返回键退出
     */
    @Override
    public void onBackPressed() {
        if (!exitFlag) {
            showToast("再按一次退出程序",0);
            exitFlag = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exitFlag = false;
                }
            }, 2000);
        } else {
            finish();
        }
    }
}
