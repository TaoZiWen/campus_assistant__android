package com.example.schoolassistant_android.base;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 所属包：com.example.schoolassistant_android.base
 * 作者：Administrator on 2020/8/18 14:46
 * 邮箱：tzw1109296630@163.com
 */
public class BaseData{
    private int code;
    private String currentTime;
    private String msg;



    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(String currentTime) {
        this.currentTime = currentTime;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "BaseData{" +
                "code=" + code +
                ", currentTime='" + currentTime + '\'' +
                ", msg='" + msg + '\'' +
                '}';
    }
}
