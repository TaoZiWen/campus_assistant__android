package com.example.schoolassistant_android.constant;

/**
 * 所属包：com.example.schoolassistant_android.config
 * 作者：Administrator on 2020/8/18 13:45
 * 邮箱：tzw1109296630@163.com
 * @Description 系统级别的配置常量
 */
public class Config {

    public static final String HOST_URL = "";

    public static final String POST = "";

    //=================系统配置key====================
    public static final String FIRST_OPEN = "FIRST_OPEN";//是否第一次打开app

    public static final String IS_LOGIN = "IS_LOGIN";//是否登录

    public static final String USER_INFO = "USER_INFO";//登录用户信息

    public static final String BACKGROUND_STYLE = "BACKGROUND_STYLE";//界面样式

    public static final String MMKV_FILE_NAME = "SCHOOL_DATA"; //保存在手机里面的文件名
}
